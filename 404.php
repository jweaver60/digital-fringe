<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(' | ', true, 'right'); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap-responsive.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome-ie7.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/animations.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<?php wp_head(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/html5shiv.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/custom.js"></script>
</head>
<body <?php body_class(); ?>>

<section class="not-found" id="not-found">
	<div class="row-fluid">
		<div class="span2">
			<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php bloginfo('template_url'); ?>/assets/img/logo.png" alt="Digital Fringe Logo" />
				<h1><span class="white">Digital</span><span class="green">Fringe</span></h1>
			</a>
		</div>
		<div class="span10">
			<h1 class="not-found-page-title">Umm, nothing here.</h1>
			<p>Error 404: Page not found.</p>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="go-home-button">Go Home</a>
		</div>
	</div>
</section>

<?php get_footer(); ?>