<?php get_header(); ?>
<section class="what-we-do" id="what-we-do">
	<?php $what_we_do = get_post(5); ?>
	<div class="row-fluid">
		<h1 class="page-title"><?php echo $what_we_do->post_title; ?></h1>
	</div>
	<div class="row-fluid border">
		<div class="span4">
			<div class="print-container">
				<div class="print-img"></div>
				<div class="print-info">
					<h2 class="hidden-header"><?php the_field('print_capabilities_title', 5); ?></h2>
					<?php the_field('print_capabilities_content', 5); ?>
				</div>
			</div>
			<h2 class="stroke" id="print-trigger">Print</h2>
		</div>
		<div class="span4">
			<div class="print-container">
				<div class="laminate-img"></div>
				<div class="laminate-info">
					<h2 class="hidden-header"><?php the_field('laminate_capabilities_title', 5); ?></h2>
					<?php the_field('laminate_capabilities_content', 5); ?>
				</div>
			</div>
			<h2 class="stroke" id="laminate-trigger">Laminate</h2>
		</div>
		<div class="span4">
			<div class="print-container">
				<div class="mounting-img"></div>
				<div class="mounting-info">
					<h2 class="hidden-header"><?php the_field('mounting_capabilities_title', 5); ?></h2>
					<?php the_field('mounting_capabilities_content', 5); ?>
				</div>
			</div>
			<h2 class="stroke" id="mounting-trigger">Mounting</h2>
		</div>
	</div>
	<h2 class="stroke" id="customer-types"><?php the_field('customer_types_title', 5); ?></h2>
	<div class="row-fluid">
		<div class="span4 center"><?php the_field('customer_types_first', 5); ?></div>
		<div class="span4 center"><?php the_field('customer_types_second', 5); ?></div>
		<div class="span4 center"><?php the_field('customer_types_third', 5); ?></div>
	</div>
	<div class="row-fluid">
		<p><?php echo $what_we_do->post_content; ?></p>
	</div>
	<div class="row-fluid">
		<h2 class="stroke">[ Over 1million square feet of capacity ]</h2>
	</div>	
</section>
<section class="who-we-are" id="who-we-are">
	<?php $who_we_are = get_post(4); ?>
	<div class="row-fluid">
		<h1 class="page-title"><?php echo $who_we_are->post_title; ?></h1>
	</div>
	<div class="row-fluid">
		<p><?php echo $who_we_are->post_content; ?></p>
	</div>
	<div class="carousel slide" id="who-we-are-carousel">
		<div class="row-fluid">
			<div class="span8 offset2">
				<nav class="who-we-are-slider">
					<ul>
						<li><a data-target="#who-we-are-carousel" data-slide-to="0">Our History</a></li>
						<li><a data-target="#who-we-are-carousel" data-slide-to="1">Philosophy</a></li>
						<li><a data-target="#who-we-are-carousel" data-slide-to="2">Focus</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="carousel-inner">
			<section class="item active" id="our-history">
				<div class="row-fluid">
					<div class="span4">
						<img src="<?php the_field('our_history_image', 4); ?>" />
					</div>
					<div class="span8">
						<h2 class="stroke"><?php the_field('our_history_subtitle', 4); ?></h2>
						<p><?php the_field('our_history_body', 4); ?></p>
					</div>
				</div>
			</section>
			<section class="item" id="philosophy">
				<div class="row-fluid">
					<div class="span4">
						<img src="<?php the_field('philosophy_image', 4); ?>" />
					</div>
					<div class="span8">
						<h2 class="stroke"><?php the_field('philosophy_subtitle', 4); ?></h2>
						<p><?php the_field('philosophy_body', 4); ?></p>
					</div>
				</div>
			</section>
			<section class="item" id="our-team">
				<div class="row-fluid">
					<div class="span4">
						<img src="<?php the_field('our_team_image', 4); ?>" />
					</div>
					<div class="span8">
						<h2 class="stroke"><?php the_field('our_team_subtitle', 4); ?></h2>
						<p><?php the_field('our_team_body', 4); ?></p>
					</div>
				</div>
			</section>	
		</div>	
	</div>
</section>
<section class="client-tools" id="client-tools">
	<?php $client_tools = get_post(7); ?>
	<div class="row-fluid">
		<h1 class="page-title"><?php echo $client_tools->post_title; ?></h1>
	</div>
	<div class="row-fluid">
		<div class="span4" id="need-to-know-container">
			<div class="client-tool">
				<div class="what-you-need-to-know"></div>
				<h3><?php the_field('what_you_need_to_know_header', 7); ?></h3>
				<p><?php the_field('what_you_need_to_know_body', 7); ?></p>
				<a class="block-link" href="#" id="need-to-know-trigger">More</a>
			</div>
		</div>
		<div class="span4" id="upload-container">
			<div class="client-tool">
				<div class="file-upload-interface"></div>
				<h3><?php the_field('file_upload_interface_header', 7); ?></h3>
				<p><?php the_field('file_upload_interface_body', 7); ?></p>
				<a class="block-link" href="#">More</a>
			</div>
		</div>
		<div class="span4" id="transfer-container">
			<div class="client-tool">
				<div class="file-transfer-options"></div>
				<h3><?php the_field('file_transfer_options_header', 7); ?></h3>
				<p><?php the_field('file_transfer_options_body', 7); ?></p>
				<a class="block-link" href="#">More</a>
			</div>
		</div>
		<div class="span8" id="need-to-know-expand" style="display: none;">
			<h2 class="hidden-header"><?php the_field('what_you_need_to_know_expanded_title', 7); ?></h2><i class="icon-remove-sign close-button"></i>
			<?php the_field('what_you_need_to_know_expanded_content', 7); ?>
		</div>
	</div>
</section>
<section class="blog-roll" id="blog-roll">
	<div class="row-fluid">
		<div class="span6 latest-news">
			<h1 class="page-title">Latest News</h1>
			<?php 
				
				query_posts('cat=3&showposts=1');
					
				while (have_posts()) : the_post(); ?>

					<?php the_post_thumbnail(); ?>
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>

			<?php endwhile; wp_reset_query(); ?>
			<div class="clear"></div>
		</div>
		<div class="span6 blog-roll-sidebar">
			<h1 class="page-title">Blog Roll</h1>
			<?php query_posts('cat=4&showposts=3'); ?>

				<div class="blog-roll-container">

				<?php while (have_posts()) : the_post(); ?>
					<div class="row-fluid sample">
						<div class="span4">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="span8">
							<h2><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; ?>

				</div>

			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>
<section class="testimonials" id="testimonials">
	<h1 class="page-title">Testimonials</h1>
	<div class="carousel slide" id="testimonials-carousel">
		<div class="row-fluid">
			<div class="span1 test-nav"><a href="#testimonials-carousel" data-slide="prev"><i class="icon-chevron-left"></i></a></div>
			<div class="span10">
				<div class="carousel-inner">
					<?php 
						$arg = array('category' => 5);
						$testimonials = wp_get_recent_posts($arg);
						$i = 0;
						foreach ($testimonials as $testimonial) { ?>
							<div class="item <?php if($i == 0) { echo 'active'; } ?>">
								<h2 class="stroke"><?php echo $testimonial['post_title']; ?></h2>
								<p><?php echo $testimonial['post_content']; ?></p>
							</div>
							<?php $i++; ?>
					<?php } ?>					
				</div>
			</div>
			<div class="span1 test-nav"><a href="#testimonials-carousel" data-slide="next"><i class="icon-chevron-right"></i></a></div>
		</div>		
	</div>
</section>
<section class="contact" id="contact">
	<?php $contact_page = get_post(63); ?>
	<div class="row-fluid">
		<h1 class="page-title"><?php echo $contact_page->post_title; ?></h1>
	</div>
	<div class="row-fluid">
		<div class="span4">
			<div class="general-info">
				<p><?php echo $contact_page->post_content; ?></p>
			</div>
		</div>
		<div class="span8">
			<?php gravity_form(1, false, false, false, true, 1); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>