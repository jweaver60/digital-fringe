<div class="row-fluid" style="height: 50px;">
	<div class="span11">
		<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" style="display: none; float: right;">
			<div id="search-inputs">
				<input type="text" value="" name="s" id="s" />
				<!-- <input type="submit" id="searchsubmit" value="Search" /> -->
			</div>
		</form>
	</div>
	<div class="span1">
		<a class="search-button" href="#"><i class="icon-search"></i></a>
	</div>
</div>

