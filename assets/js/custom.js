$(document).ready(function() {

  // jQuery .toggle() method is deprecated, reintroduce function here!
  jQuery.fn.toggle = function( fn, fn2 ) {
    // Don't mess with animation or css toggles
    if ( !jQuery.isFunction( fn ) || !jQuery.isFunction( fn2 ) ) {
      return oldToggle.apply( this, arguments );
    }
    // migrateWarn("jQuery.fn.toggle(handler, handler...) is deprecated");
    // Save reference to arguments for access in closure
    var args = arguments,
    guid = fn.guid || jQuery.guid++,
    i = 0,
    toggler = function( event ) {
      // Figure out which function to execute
      var lastToggle = ( jQuery._data( this, "lastToggle" + fn.guid ) || 0 ) % i;
      jQuery._data( this, "lastToggle" + fn.guid, lastToggle + 1 );
      // Make sure that clicks stop
      event.preventDefault();
      // and execute the function
      return args[ lastToggle ].apply( this, arguments ) || false;
    };
    // link all the functions, so any of them can unbind this click handler
    toggler.guid = guid;
    while ( i < args.length ) {
      args[ i++ ].guid = guid;
    }
    return this.click( toggler );
  };

  // Hide slider labels on page load
	$('.slider-label').hide();

  // On hover show slider labels
	$('.slider-nav a').hover(function() {
		$(this).find('.slider-label').show();
	}, function() {
		$(this).find('.slider-label').hide();
	});

  // Smooth page scrolling for top navigation
	$(".scroll").click(function(event){
    event.preventDefault();
    //calculate destination place
    var dest=0;
    if($(this.hash).offset().top > $(document).height()-$(window).height()){
      dest=$(document).height()-$(window).height();
    }else{
      dest=$(this.hash).offset().top;
    }
    //go to destination
    $('html,body').animate({scrollTop:dest}, 1000,'swing');
  });

  // Show search form on icon click
  $('a.search-button').click(function() {
    $('#searchform').animate({width: 'toggle'});
  });

  // Slide in info panels for What We Do section
  function handlePanels(selector, image) {
    selector.click(function() {
      if (image.css('margin-left') === '0px') {
        image.animate({ marginLeft: '-100%' });
      } else {
        image.animate({ marginLeft: '0' });
      }
    });
    image.click(function() {
      image.animate({ marginLeft: '-100%' });
    });
  }

  handlePanels($('#print-trigger'), $('.print-img'));

  handlePanels($('#laminate-trigger'), $('.laminate-img'));

  handlePanels($('#mounting-trigger'), $('.mounting-img'));

  $('#need-to-know-trigger').click(function(e) {
    e.preventDefault();
    $('#upload-container').fadeOut();
    $('#transfer-container').fadeOut();
    setTimeout(function() {
      $('#need-to-know-expand').fadeIn();
    },750);
  });

  $('.close-button').click(function() {
    $('#need-to-know-expand').fadeOut();
    setTimeout(function() {
      $('#upload-container').fadeIn();
      $('#transfer-container').fadeIn();
    }, 750);
  });

  // scroll body to 0px on click
    $('.back-to-top').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });

    $('.blog-roll-container a').attr('href', '/digitalfringe/blog');
    $('.search-results .span12 a').attr('href', '/digitalfringe/blog');

});