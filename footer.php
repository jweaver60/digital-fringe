	<footer>
		<div class="row-fluid">
			<div class="span4">
				<div id="copyright">
					<?php echo sprintf( __( '%1$s %2$s %3$s', 'blankslate' ), '&copy;', date('Y'), esc_html(get_bloginfo('name')) ); ?>
				</div>
			</div>
			<div class="span3">

				<a href="#privacyModal" data-toggle="modal" role="button" class="privacy">Privacy Policy</a>
 
				<!-- Modal -->
				<div id="privacyModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel" aria-hidden="true">
				  <div class="modal-header">
				  	<?php $privacy_page = get_page(83); ?>
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel"><?php echo $privacy_page->post_title; ?></h3>
				  </div>
				  <div class="modal-body">
				    <p><?php echo apply_filters('the_content', $privacy_page->post_content); ?></p>
				  </div>
				</div>

			</div>
			<div class="span5">
				<div class="row-fluid">
					<div class="span4">
						<p>Social Media</p>
					</div>
					<div class="span8">
						<a href="#" class="icon"><i class="icon-facebook"></i></a><a href="#" class="icon"><i class="icon-twitter"></i></a><a href="#" class="icon"><i class="icon-linkedin"></i></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>