<?php
/*
Template Name: Blog Page Template
*/
?>

<?php get_header(); ?>

<div class="row-fluid blog-template-page">
		<h1 class="page-title"><?php the_title(); ?></h1>
		<?php query_posts('cat=4&showposts=-1'); ?>
			<?php while (have_posts()) : the_post(); ?>
				<div class="row-fluid">
					<div class="span3">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="span9 with-bottom-border">
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					</div>
				</div>
			<?php endwhile; ?>
		<?php wp_reset_query(); ?>
</div>

<?php get_footer(); ?>