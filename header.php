<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(' | ', true, 'right'); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap-responsive.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome-ie7.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/animations.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<?php wp_head(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/html5shiv.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/custom.js"></script>
</head>
<body <?php body_class(); ?>>
	<header>
		<div class="row-fluid">
			<a href="#" class="back-to-top"><i class="icon-chevron-up"></i></a>
			<div class="span3">
				<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php bloginfo('template_url'); ?>/assets/img/logo.png" alt="Digital Fringe Logo" />
					<h1><span class="white">Digital</span><span class="green">Fringe</span></h1>
				</a>
			</div>
			<div class="span7 offset1">
				<nav class="main-nav">
					<div id="search">
						<?php get_search_form(); ?>
					</div>
					<div class="menu-main-navigation-container">
						<ul id="menu-main-navigation" class="menu">
							<li><a href="<?php render_correct_link('#who-we-are'); ?>" <?php scroll_class(); ?>>Who We Are</a></li>
							<li><a href="<?php render_correct_link('#what-we-do'); ?>" <?php scroll_class(); ?>>What We Do</a></li>
							<li><a href="<?php render_correct_link('#contact'); ?>" <?php scroll_class(); ?>>Contact</a></li>
							<li><a href="<?php render_correct_link('#client-tools'); ?>" <?php scroll_class(); ?>>Client Tools</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<div class="row-fluid">
			<div class="text-container">
				<img src="<?php bloginfo('template_url'); ?>/assets/img/capabilities.png" alt="Capabilities" />
			</div>
			<div class="row-fluid">
				<div class="span3 offset9">
					<div class="saving-butts">
						<img src="<?php bloginfo('template_url'); ?>/assets/img/saving_butts.png" alt="Saving Butts since 1999" />
					</div>
				</div>
			</div>
		</div>
		<div class="slider-nav">
			<div class="row-fluid">
				<ul>
					<li><a href="<?php render_correct_link('#what-we-do'); ?>" <?php scroll_class(); ?>><span class="slider-label">Capabilities</span><i class="icon-tint"></i></a></li>
					<li><a href="<?php render_correct_link('#who-we-are'); ?>" <?php scroll_class(); ?>><span class="slider-label">About Us</span><i class="icon-star"></i></a></li>
					<li><a href="<?php render_correct_link('#customer-types'); ?>" <?php scroll_class(); ?>><span class="slider-label">Customers</span><i class="icon-group"></i></a></li>
					<li><a href="<?php render_correct_link('#client-tools'); ?>" <?php scroll_class(); ?>><span class="slider-label">Client Tools</span><i class="icon-crop"></i></a></li>
					<li><a href="<?php render_correct_link('#blog-roll'); ?>" <?php scroll_class(); ?>><span class="slider-label">Latest News</span><i class="icon-bullhorn"></i></a></li>
					<li><a href="<?php render_correct_link('#testimonials'); ?>" <?php scroll_class(); ?>><span class="slider-label">Testimonials</span><i class="icon-comment-alt"></i></a></li>
					<li><a href="<?php render_correct_link('#contact'); ?>" <?php scroll_class(); ?>><span class="slider-label">Reach Out</span><i class="icon-envelope-alt"></i></a></li>
				</ul>
			</div>
		</div>
	</header>