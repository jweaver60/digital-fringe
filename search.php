<?php get_header(); ?>

<div class="row-fluid search-results">
	<?php if ( have_posts() ) : ?>
		<h1 class="search-results-page-title"><?php printf( __( 'Search Results for: %s', 'blankslate' ), '<span>' . get_search_query()  . '</span>' ); ?></h1>
		<div class="span12">
			<h2 class="stroke">Search results in Posts</h2>
			<?php 
				global $post; 
				rewind_posts();
				$query = new WP_Query(array(
					's' => get_search_query(),
 					'posts_per_page' => 3,
 					'post_type' => 'post'
				));
				while ($query->have_posts()) : $query->the_post(); 
			?>
			
				<?php get_template_part( 'entry' ); ?>

			<?php 
			endwhile; 
			wp_reset_postdata();
			?>

		</div>
	<?php else : ?>
		<div id="post-0" class="post no-results not-found">
			<h2 class="entry-title"><?php _e( 'Nothing Found', 'blankslate' ) ?></h2>
			<div class="entry-content">
				<p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php get_footer(); ?>